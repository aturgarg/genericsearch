﻿namespace GenericSearch.Application.Context
{
    public interface IElasticConnectionContext
    {
        string IndexName { get; }

        string EndPointUrl { get; }
    }
}
