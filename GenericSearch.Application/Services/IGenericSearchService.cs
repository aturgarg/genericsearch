﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenericSearch.Application.Services
{
    public interface IGenericSearchService<T> where T : class
    {
        CreateResponse Add(T item);

        Task<CreateResponse> AddAsync(T item);

        BulkResponse Add(List<T> items);

        Task<IReadOnlyCollection<BulkResponseItemBase>> AddAsync(List<T> items);

        T Get(string id);

        Task<T> GetAsync(string id);

        IUpdateResponse<T> Update(T item);

        Task<IUpdateResponse<T>> UpdateAsync(T item);

        DeleteResponse Delete(string id);

        Task<DeleteResponse> DeleteAsync(string id);

        IReadOnlyCollection<BulkResponseItemBase> Delete(List<T> items);

        Task<IReadOnlyCollection<BulkResponseItemBase>> DeleteAsync(List<T> items);

        IReadOnlyCollection<T> Search(string searchText, int from, int size, Expression<Func<T, object>> fieldToSearchFrom);

        Task<IReadOnlyCollection<T>> SearchAsync(string searchText, int from, int size, Expression<Func<T, object>> fieldToSearchFrom);

        IReadOnlyCollection<T> Search(string searchText, int from, int size);

        Task<IReadOnlyCollection<T>> SearchAsync(string searchText, int from, int size);
    }
}
