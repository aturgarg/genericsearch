﻿using GenericSearch.Application.Services;
using GenericSearch.Infra;
using GenericSearch.Infra.ElasticSearchProvider;
using GenericSearch.Infra.ServicesImpl;
using Microsoft.Extensions.DependencyInjection;

namespace GenericSearch
{
    public static class GenericElasticSearchExtension
    {
        public static IServiceCollection AddGenericElasticSearch(this ServiceCollection services)
        {
            services
                .AddGenericElasticSearchInfrastructure()               
                ;

            return services;
        }
    }
}
