﻿using GenericSearch.Application.Context;
using GenericSearch.Application.Services;
using GenericSearch.Infra.ElasticSearchProvider;
using GenericSearch.Infra.ServicesImpl;
using Microsoft.Extensions.DependencyInjection;

namespace GenericSearch.Infra
{
    public static class GenericSearchInfrastructureExtension
    {
        public static IServiceCollection AddGenericElasticSearchInfrastructure(this ServiceCollection services)
        {
            services
                .AddSingleton<IElasticSearchClientFactory, ElasticSearchClientFactory>()
                .AddSingleton<IElasticConnectionContext, ElasticConnectionContext>()
                //.AddTransient<IGenericSearchService<object>, GenericElasticSearchService<object>>()
                .AddTransient(typeof(IGenericSearchService<>), typeof(GenericElasticSearchService<>))
                 ;

            return services;
        }
    }
}
