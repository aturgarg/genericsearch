﻿using GenericSearch.Application.Context;
using GenericSearch.Application.Services;
using GenericSearch.Infra.ElasticSearchProvider;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenericSearch.Infra.ServicesImpl
{
    public class GenericElasticSearchService<T> : IGenericSearchService<T> where T : class
    {
        private readonly IElasticClient _elasticClient;
        private readonly IElasticSearchClientFactory _elasticSearchClientFactory;
        private readonly IElasticConnectionContext _elasticConnectionContext;
      
        public GenericElasticSearchService(IElasticConnectionContext elasticConnectionContext,
                                           IElasticSearchClientFactory elasticSearchClientFactory)
        {
            _elasticConnectionContext = elasticConnectionContext;
            _elasticSearchClientFactory = elasticSearchClientFactory;
            _elasticClient = _elasticSearchClientFactory.GetElasticClient(_elasticConnectionContext);
        }

        public CreateResponse Add(T item)
        {
            try
            {                
                return _elasticClient.Create<T>(new CreateRequest<T>(item));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<CreateResponse> AddAsync(T item)
        {
            try
            {              
                var response = await _elasticClient.CreateAsync<T>(new CreateRequest<T>(item));
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BulkResponse Add(List<T> items)
        {
            try
            {
                return _elasticClient.IndexMany(items);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IReadOnlyCollection<BulkResponseItemBase>> AddAsync(List<T> items)
        {
            try
            {
                var response = await _elasticClient.IndexManyAsync(items);
                return response.Items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Get(string id)
        {
            try
            {
                return _elasticClient.Get<T>(id).Source;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<T> GetAsync(string id)
        {
            try
            {
                var response = await _elasticClient.GetAsync<T>(id);
                return response.Source;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IUpdateResponse<T> Update(T item)
        {
            try
            {
                var response = _elasticClient.Update(new DocumentPath<T>(item), x => x.Doc(item));
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IUpdateResponse<T>> UpdateAsync(T item)
        {
            try
            {
                var response = await _elasticClient.UpdateAsync(new DocumentPath<T>(item), x => x.Doc(item));
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DeleteResponse Delete(string id)
        {
            try
            {
                return _elasticClient.Delete(new DeleteRequest<T>(id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public async Task<DeleteResponse> DeleteAsync(string id)
        {
            try
            {
                var response = await _elasticClient.DeleteAsync(new DeleteRequest<T>(id));
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public IReadOnlyCollection<BulkResponseItemBase> Delete(List<T> items)
        {
            try
            {
                return _elasticClient.DeleteMany(items).Items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public async Task<IReadOnlyCollection<BulkResponseItemBase>> DeleteAsync(List<T> items)
        {
            try
            {
                var response = await _elasticClient.DeleteManyAsync(items);
                return response.Items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public IReadOnlyCollection<T> Search(string searchText, int inclusiveFrom, int size, Expression<Func<T, object>> fieldToSearchFrom)
        {
            try
            {
                var response = _elasticClient.Search<T>(s => s
                                         .From(inclusiveFrom) 
                                         .Size(size)
                                         .Query(q => q.Match(mq => mq.Field(fieldToSearchFrom).Query(searchText.ToLower())))
                                         );

                return response.Documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public async Task<IReadOnlyCollection<T>> SearchAsync(string searchText, int inclusiveFrom, int size, Expression<Func<T, object>> fieldToSearchFrom)
        {
            try
            {
                var response = await _elasticClient.SearchAsync<T>(s => s
                                         .From(inclusiveFrom) 
                                         .Size(size)
                                         .Query(q => q.Match(mq => mq.Field(fieldToSearchFrom).Query(searchText.ToLower())))
                                         );

                return response.Documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public IReadOnlyCollection<T> Search(string searchText, int inclusiveFrom, int size)
        {
            try
            {
                var response = _elasticClient.Search<T>(s => s
                                      .From(inclusiveFrom) 
                                      .Take(size)
                                      .Query(q => q.Bool(b => b.Must(m => m.QueryString(qs => qs.DefaultField("_all").Query(searchText)))))
                                       );

                return response.Documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public async Task<IReadOnlyCollection<T>> SearchAsync(string searchText, int inclusiveFrom, int size)
        {
            try
            {
                var response = await _elasticClient.SearchAsync<T>(s => s
                                           .From(inclusiveFrom)
                                           .Take(size)
                                           .Query(q => q.Bool(b => b.Must(m => m.QueryString(qs => qs.DefaultField("_all").Query(searchText)))))
                                           );

                return response.Documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
