﻿using GenericSearch.Application.Context;
using Nest;

namespace GenericSearch.Infra.ElasticSearchProvider
{
    public interface IElasticSearchClientFactory
    {
        IElasticClient GetElasticClient(IElasticConnectionContext elasticConnectionContext);
    }
}
