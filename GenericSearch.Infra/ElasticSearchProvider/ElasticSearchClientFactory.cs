﻿using GenericSearch.Application.Context;
using Nest;
using System;
using System.Collections.Generic;

namespace GenericSearch.Infra.ElasticSearchProvider
{
    internal class ElasticSearchClientFactory : IElasticSearchClientFactory, IDisposable
    {
        private readonly object _clientLock = new object();
        private readonly IDictionary<string, IElasticClient> _elasticClientCollection = new Dictionary<string, IElasticClient>();
        
        public void Dispose()
        {
            _elasticClientCollection.Clear();
        }

        public IElasticClient GetElasticClient(IElasticConnectionContext elasticConnectionContext)
        {
            lock (_clientLock)
            {
                // Key should be on IndexName (i.e. Database name in RDBMS term)
                string key = elasticConnectionContext.IndexName;

                if (_elasticClientCollection.TryGetValue(key, out IElasticClient elasticClient))
                    return elasticClient;
                else
                {
                    elasticClient = CreateElasticClient(elasticConnectionContext);
                    _elasticClientCollection.Add(key, elasticClient);
                    return elasticClient;
                }
            }
        }

        private IElasticClient CreateElasticClient(IElasticConnectionContext elasticConnectionContext)
        {
            var url = new Uri(elasticConnectionContext.EndPointUrl);
            var settings = new ConnectionSettings(url)
                            .DefaultIndex(elasticConnectionContext.IndexName)
                            ;
            return new ElasticClient(settings);
        }
    }
}
