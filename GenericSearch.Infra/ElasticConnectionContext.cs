﻿using GenericSearch.Application.Context;

namespace GenericSearch.Infra
{
    public class ElasticConnectionContext : IElasticConnectionContext
    {
        public string IndexName => "backup_pics1";

        public string EndPointUrl => "http://localhost:9200/";
    }
}
