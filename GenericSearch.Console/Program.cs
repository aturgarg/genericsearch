﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using GenericSearch.Application.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace GenericSearch.Console
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        public static void Main(string[] args)
        {
            RegisterServices();

            var test = new TestFile { FileName = "myfirstfile.txt" };

            var elasticService = _serviceProvider.GetRequiredService<IGenericSearchService<TestFile>>();

          

            elasticService.Add(test);

            System.Console.WriteLine("Hello World!");

            System.Console.ReadLine();
            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddGenericElasticSearch();

            var builder = new ContainerBuilder();

            builder.Populate(services);
            IContainer appContainer = builder.Build();

            _serviceProvider = new AutofacServiceProvider(appContainer);
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }


        private static void RegisterGenericSearchAssemblies(ContainerBuilder builder)
        {
            var assemblies = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory,
                                                        "GenericSearch*.dll", SearchOption.TopDirectoryOnly)
                                                .Where(filePath => Path.GetFileName(filePath).StartsWith("GenericSearch"))
                                                .Select(Assembly.LoadFrom);


            builder.RegisterAssemblyTypes(assemblies.ToArray())
               .AsImplementedInterfaces();
        }

        public class TestFile
        {
            public string FileName { get; set; }
        }
    }
}
